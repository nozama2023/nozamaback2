package eftl3.nozamaBack2.model;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


@Entity
@Table(name="user")
public class User implements Serializable{

	private static final long serialVersionUID = 5852178772091462419L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "lastname", length = 100)
	private String lastname;
	
	@Column(name = "firstname", length = 100)
	private String firstname;
	
	@Column(name = "gender", length = 100)
	private String gender;
	
	@Column(name = "mobile", length = 10)
	private Integer phone;
	
	@Column(name = "mail", length = 100)
	private String mail;
	
	@Column(name = "birthdate")
	private Date birthdate;
	
	@Column(name = "password", length = 100)
	private String password;
	
	
	@Column(name = "signInDate")
	private Date signinDate;
	
	@Column(name = "role", length = 100)
	private String role;

	
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(Integer id, String lastname, String firstname, String gender, Integer phone, String mail,
			Date birthdate, String password, Date signinDate, String role) {
		super();
		this.id = id;
		this.lastname = lastname;
		this.firstname = firstname;
		this.gender = gender;
		this.phone = phone;
		this.mail = mail;
		this.birthdate = birthdate;
		this.password = password;
		this.signinDate = signinDate;
		this.role = role;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getSigninDate() {
		return signinDate;
	}

	public void setSigninDate(Date signinDate) {
		this.signinDate = signinDate;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "user [id=" + id + ", lastname=" + lastname + ", firstname=" + firstname + ", gender=" + gender
				+ ", phone=" + phone + ", mail=" + mail + ", birthdate=" + birthdate + ", password=" + password
				+ ", signinDate=" + signinDate + ", role=" + role + "]";
	}
	

	
	

}
