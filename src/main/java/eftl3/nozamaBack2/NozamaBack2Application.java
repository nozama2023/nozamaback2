package eftl3.nozamaBack2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NozamaBack2Application {

	public static void main(String[] args) {
		SpringApplication.run(NozamaBack2Application.class, args);
	}

}
