package eftl3.nozamaBack2.service;


import java.util.Optional;

import eftl3.nozamaBack2.model.User;

public interface UserService {

Optional<User> getById(Integer id);
public User saveUser(User user);
public void deleteUser(User user);
Iterable<User> findAll();

}
