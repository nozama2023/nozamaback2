package eftl3.nozamaBack2.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import eftl3.nozamaBack2.dao.UserDao;
import eftl3.nozamaBack2.model.User;



@Service
public class UserServiceImpl implements UserService{
	
	private UserDao userDao ;


	@Override
	public User saveUser(User user) {
		// TODO Auto-generated method stub
		return userDao.save(user);
	}

	@Override
	public void deleteUser(User user) {
		// TODO Auto-generated method stub
		userDao.delete(user);
	}

	@Override
	public Iterable<User> findAll() {
		// TODO Auto-generated method stub
		return userDao.findAll();
	}

	@Override
	public Optional<User> getById(Integer id) {
		// TODO Auto-generated method stub
		return userDao.findById(id);
	}
}
