package eftl3.nozamaBack2.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eftl3.nozamaBack2.model.User;
import eftl3.nozamaBack2.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/users")
	public ResponseEntity<List<User>> getUsers(){
		List<User> users = (List<User>) userService.findAll();
			return ResponseEntity.ok()
					.header("Custom-Header", "foo")
					.body(users);

	}
	
	@GetMapping("/users/{id}")
	public ResponseEntity<Optional<User>> getById(@PathVariable Integer id){
		return ResponseEntity.ok()
				.header("Custom-Header", "foo")
				.body(userService.getById(id));
	}
	
	@PostMapping("/users")
	public ResponseEntity<User> createUser(@RequestBody User user){
		
		return ResponseEntity.ok().body(userService.saveUser(user));
	}
	
	
    @PutMapping("/users/{id}")
    public ResponseEntity<User> updatedUser(@PathVariable("id") int id, @RequestBody User user) {
    	user.setId(id);
        User updatedUser = userService.saveUser(user);
        if (updatedUser != null) {
            return ResponseEntity.ok(updatedUser);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
	
	@DeleteMapping("/users/{id}")
	public ResponseEntity<User> deteleUser(@PathVariable(value ="id") Integer id){
		Optional<User> optionalUser = userService.getById(id);
		
		if (optionalUser.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		userService.deleteUser(optionalUser.get());
		return ResponseEntity.noContent().build();
	}

}
