package eftl3.nozamaBack2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eftl3.nozamaBack2.model.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer>{
	

}
